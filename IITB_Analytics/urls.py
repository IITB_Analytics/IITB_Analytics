"""IITBXAnalysis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns,include, url
from django.contrib import admin
from login.views import *
from managerapp.views import*

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'django.contrib.auth.views.login'),
    #url(r'^$','login.views.get_multi_roles',name='get_multi_roles'),
    url(r'^logout/$', logout_page),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'), # If user is not login it will redirect to login page
    url(r'^homepage/$','login.views.homepage',name='homepage'),
    url(r'^home/$', 'login.views.home',name='home'),
    url(r'coursedesc/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)$', 'login.views.coursedesc', name='coursedesc'),
    url(r'^participantdetails/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/$','login.views.studentdetails',name='studentdetails'),
    url(r'^coursedetails/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/$','login.views.coursedetails',name='coursedetails'),
    url(r'^manager/', include('managerapp.urls'),name="managerapp"),
    url(r'^evaluationtdetails/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/$','login.views.evaluationdetails',name='evaluationdetails'),
    url(r'^quizdata/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/$','login.views.quizdata',name='quizdata'),
    url(r'^downloadquizcsv/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+|-[0-9])/$','login.views.downloadquizcsv',name='downloadquizcsv'),
    url(r'^downloadcsv/(?P<courseid>[\w{}\.\-\/]{1,40})/$','login.views.downloadcsv',name='downloadcsv'),
    url(r'^evaluationstatus/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/(?P<evalflag>[0-9])/$','login.views.evaluationstatus',name='evaluationstatus'),
    url(r'^studentstatus/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+|-[0-9])/(?P<report>[0-9])/$','login.views.studentstatus',name='studentstatus'),
    url(r'^downloadstatucsv/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+)/(?P<report>[0-9])/$','login.views.downloadstatucsv',name='downloadstatuscsv'),
    url(r'^grades/(?P<courseid>[\w{}\.\-\/]{1,40})/(?P<pid>[0-9]+|-[0-9])/$','login.views.grades_report',name='grades_report'),
    url(r'^get_course/','login.views.get_course',name='get_course'),
    url(r'^course_chapter','login.views.course_chapter',name='course_chapter'),
    url(r'^chapter_sequential','login.views.chapter_sequential',name='chapter_sequential'),
    url(r'^sequential_vertical','login.views.sequential_vertical',name='sequential_vertical'),
    url(r'^vertical_module','login.views.vertical_module',name='vertical_module'),
    url(r'^cousremodules/','login.views.get_course',name='get_module'),
    url(r'^manager/reporthome$', 'managerapp.views.reporthome',name='reporthome'),
    url(r'^manager/usersummary$', 'managerapp.views.usersummary',name="usersummary"),
    url(r'^manager/registrationsummary$', 'managerapp.views.registrationsummary',name='registrationsummary'),
    url(r'^manager/userjoinedsummary$', 'managerapp.views.userjoinedsummary',name='userjoinedsummary'),
    url(r'^manager/coursesummary$', 'managerapp.views.coursesummary',name='coursesummary'),




]

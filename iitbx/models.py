from django.db import models
from datetime import datetime,time
from datetime import date
from django.utils import timezone
from time import time
from django.utils.timezone import *
from django.contrib.auth.models import User
from models import *

# Create your models here.

def get_upload_image(instance,filename):
	return "static/upload/upload_images/%s" % filename

class edxcourses(models.Model):   
    tag =  models.CharField(max_length=100,null=True)
    org =  models.CharField(max_length=100,null=True)
    course =  models.CharField(max_length=100,null=True)
    name =  models.CharField(max_length=100,null=True)
    courseid =  models.CharField(max_length=100,null=True, unique=True)
    coursename=models.CharField(max_length=100, null=True)
    enrollstart =  models.DateTimeField(null=True)
    enrollend =  models.DateTimeField(null=True)
    coursestart =  models.DateTimeField(null=True)
    courseend =  models.DateTimeField(null=True)
    image=models.ImageField(upload_to=get_upload_image)
    instructor=models.CharField(max_length=50)
    coursesubtitle=models.TextField()
    class Meta:
        managed = True

class coursefaculty(models.Model):
    person = models.ForeignKey(User)
    course = models.ForeignKey(edxcourses) 

class course_modlist(models.Model):
    display_name= models.CharField(max_length=200)
    module_type= models.CharField(max_length=200)
    module_id= models.CharField(max_length=200)
    related_id= models.IntegerField()


class EmailContent(models.Model):
	systype=models.CharField(max_length=30)
	name=models.CharField(max_length=100)
	subject=models.CharField(max_length=100)
	message=models.TextField()

class ErrorContent(models.Model):
	systype=models.CharField(max_length=30)
	name=models.CharField(max_length=100)
	errorcode=models.CharField(max_length=20, unique=True, default='null')
	error_message=models.TextField()

class evaluations(models.Model):
     course =models.ForeignKey(edxcourses)
     sectionid =models.CharField(max_length=250) 
     sec_name  =models.CharField(max_length=250)
     subsec_id =models.CharField(max_length=250)
     subsec_name =models.CharField(max_length=250)
     type =models.CharField(max_length=150)
     release_date =models.DateTimeField()
     due_date =models.DateTimeField()
     total_weight =models.FloatField()
     grade_weight =models.FloatField()    
     total_marks=models.IntegerField()  


class gradepolicy(models.Model):
    	courseid =  models.ForeignKey(edxcourses, to_field='courseid')  #foreign key with edxcourses
    	min_count=models.IntegerField(null=True)
    	weight=models.FloatField(null=True)
    	type=models.CharField(max_length=100,null=True)
    	drop_count=models.IntegerField(null=True)
    	short_label=models.CharField(max_length=10,null=True)

class gradescriteria(models.Model):
    	courseid =  models.ForeignKey(edxcourses, to_field='courseid')  #foreign key with edxcourses
    	grade=models.CharField(max_length=5,null=True)
    	cutoffs=models.FloatField(null=True)

class gradestable(models.Model):
	edx_user = models.IntegerField(null=True)
        course = models.CharField(max_length=250)
        grade =models.CharField(max_length=25)
        eval = models.TextField(null=True)   

class headings(models.Model):
      section=models.CharField(max_length=200)
      heading=models.TextField()

class Lookup(models.Model): # remove T10KT , make it just Lookup 
	category=models.CharField(max_length=75,null=False)
	code=models.IntegerField(null=False)
	description=models.CharField(max_length=100, null=False)
	comment=models.CharField(max_length=100,null=True)
	is_active = models.BooleanField(default=1)

class markstable(models.Model):
	edx_user = models.IntegerField(null=True)
        section= models.CharField(max_length=250)
        total=models.CharField(max_length=25)
        eval = models.TextField(null=True)

class questions(models.Model):
      course =models.ForeignKey(edxcourses)
      eval =models.ForeignKey(evaluations)
      qid =models.CharField(max_length=250) 
      q_name = models.CharField(max_length=250)    
      q_weight= models.FloatField() 

class Reports(models.Model):
	reportid=models.CharField(max_length=20,primary_key=True)
	usertype=models.IntegerField()
	sqlquery=models.CharField(max_length=200)
	report_title=models.CharField(max_length=100)
	num_cols=models.IntegerField()
	comments=models.CharField(max_length=200)
	category=models.CharField(max_length=30)
	rel_rep_id=models.IntegerField()

class result(models.Model):
       question =models.ForeignKey(questions)
       edxuserid =models.IntegerField()
       grade =models.FloatField()
       maxgrade =models.FloatField()

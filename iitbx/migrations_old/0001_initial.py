# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import iitbx.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='course_modlist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('display_name', models.CharField(max_length=200)),
                ('module_type', models.CharField(max_length=200)),
                ('module_id', models.CharField(max_length=200)),
                ('related_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='coursefaculty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='edxcourses',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=100, null=True)),
                ('org', models.CharField(max_length=100, null=True)),
                ('course', models.CharField(max_length=100, null=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('courseid', models.CharField(max_length=100, unique=True, null=True)),
                ('coursename', models.CharField(max_length=100, null=True)),
                ('enrollstart', models.DateTimeField(null=True)),
                ('enrollend', models.DateTimeField(null=True)),
                ('coursestart', models.DateTimeField(null=True)),
                ('courseend', models.DateTimeField(null=True)),
                ('image', models.ImageField(upload_to=iitbx.models.get_upload_image)),
                ('instructor', models.CharField(max_length=50)),
                ('coursesubtitle', models.TextField()),
            ],
            options={
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='EmailContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('systype', models.CharField(max_length=30)),
                ('name', models.CharField(max_length=100)),
                ('subject', models.CharField(max_length=100)),
                ('message', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='ErrorContent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('systype', models.CharField(max_length=30)),
                ('name', models.CharField(max_length=100)),
                ('errorcode', models.CharField(default=b'null', unique=True, max_length=20)),
                ('error_message', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='evaluations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sectionid', models.CharField(max_length=250)),
                ('sec_name', models.CharField(max_length=250)),
                ('subsec_id', models.CharField(max_length=250)),
                ('subsec_name', models.CharField(max_length=250)),
                ('type', models.CharField(max_length=150)),
                ('release_date', models.DateTimeField()),
                ('due_date', models.DateTimeField()),
                ('total_weight', models.FloatField()),
                ('grade_weight', models.FloatField()),
                ('total_marks', models.IntegerField()),
                ('course', models.ForeignKey(to='iitbx.edxcourses')),
            ],
        ),
        migrations.CreateModel(
            name='gradepolicy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('min_count', models.IntegerField(null=True)),
                ('weight', models.FloatField(null=True)),
                ('type', models.CharField(max_length=100, null=True)),
                ('drop_count', models.IntegerField(null=True)),
                ('short_label', models.CharField(max_length=10, null=True)),
                ('courseid', models.ForeignKey(to='iitbx.edxcourses', to_field=b'courseid')),
            ],
        ),
        migrations.CreateModel(
            name='gradescriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('grade', models.CharField(max_length=5, null=True)),
                ('cutoffs', models.FloatField(null=True)),
                ('courseid', models.ForeignKey(to='iitbx.edxcourses', to_field=b'courseid')),
            ],
        ),
        migrations.CreateModel(
            name='gradestable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edx_user', models.IntegerField(null=True)),
                ('course', models.CharField(max_length=250)),
                ('grade', models.CharField(max_length=25)),
                ('eval', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='headings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('section', models.CharField(max_length=200)),
                ('heading', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Lookup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=75)),
                ('code', models.IntegerField()),
                ('description', models.CharField(max_length=100)),
                ('comment', models.CharField(max_length=100, null=True)),
                ('is_active', models.BooleanField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='markstable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edx_user', models.IntegerField(null=True)),
                ('section', models.CharField(max_length=250)),
                ('total', models.CharField(max_length=25)),
                ('eval', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='questions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('qid', models.CharField(max_length=250)),
                ('q_name', models.CharField(max_length=250)),
                ('q_weight', models.FloatField()),
                ('course', models.ForeignKey(to='iitbx.edxcourses')),
                ('eval', models.ForeignKey(to='iitbx.evaluations')),
            ],
        ),
        migrations.CreateModel(
            name='Reports',
            fields=[
                ('reportid', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('usertype', models.IntegerField()),
                ('sqlquery', models.CharField(max_length=200)),
                ('report_title', models.CharField(max_length=100)),
                ('num_cols', models.IntegerField()),
                ('comments', models.CharField(max_length=200)),
                ('category', models.CharField(max_length=30)),
                ('rel_rep_id', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='coursefaculty',
            name='course',
            field=models.ForeignKey(to='iitbx.edxcourses'),
        ),
        migrations.AddField(
            model_name='coursefaculty',
            name='person',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('iitbx', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('edxuserid', models.IntegerField()),
                ('grade', models.FloatField()),
                ('maxgrade', models.FloatField()),
                ('question', models.ForeignKey(to='iitbx.questions')),
            ],
        ),
    ]

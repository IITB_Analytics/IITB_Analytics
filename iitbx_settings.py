import pymongo
import MySQLdb
import argparse,re,datetime
import sys, getopt,os
import django
import time
from datetime import date, timedelta
#Please add the full project folder
project_dir="IITBXAnalysis" 
sys.path.append(project_dir)
os.environ['DJANGO_SETTINGS_MODULE']='IITBXAnalysis.settings'
django.setup()


from pymongo import MongoClient
from django.db import models,transaction
from django.core.mail.message import EmailMultiAlternatives
from iitbx.models import *
from login.models import *


mysql_host="localhost"
#please enter mysq username password
user="root"
passwd="root"
mysql_schema="edxapp"
mongodb='mongodb://10.105.25.130:27017/'
#courses=["CS101.1xA15","ME209xA15","EE210.1xA15"]

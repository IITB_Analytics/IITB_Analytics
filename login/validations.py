from .models import *



def getErrorContent(errorcode):
  try:
     return ErrorContent.objects.get(errorcode=errorcode).error_message
  except:
     return "Error Message not defined"


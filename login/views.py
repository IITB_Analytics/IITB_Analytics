from django.shortcuts import render,render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.http import HttpResponseRedirect,HttpResponse
from .models import *
from iitbx.models import *
from validations import *
from globalss import *
import MySQLdb
import random
from datetime import date,timedelta
from login.models import *
from datetime import date,timedelta
from time import time
from django.core.context_processors import csrf
from django.template import RequestContext
#from easygui import *
import csv
import os


current=timezone.now
# Create your views here.

##############################################login & logout##########################################################################
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')
 
@login_required
def homepage(request):
     args={}
     args['institutename']=request.session['org']
     args['email']=request.user
     return render_to_response('homepage.html',args)


def home(request):
    email=request.user
    auth_list=AuthUser.objects.raw('''select distinct ea.id  id, eca.course_id courseid  ,ea.username as username, eca.org as org from edxapp.auth_user ea , edxapp.student_courseaccessrole eca where eca.user_id=ea.id and ea.email=%s''',[email] )
    #auth_list=mysql_csr.fetchall()
    course=[]
    args={}
    for i in auth_list:
        course.append([i.id,i.courseid])
        username=i.username
        organisation=i.org
        
        args['courselist']=course  
        args['email']=request.user
        args['user']=username
        args['institutename']=request.session['org']=organisation
    return render_to_response('home.html',args)
##################################################### end of login & logour#############################################################
@login_required
def coursedesc(request,courseid,pid):
     args={}
     args['institutename']=request.session['org']
     args['courseid']=courseid
     course=edxcourses.objects.get(courseid=courseid).course
     args['course']=course
     args['email']=request.user
     args['pid']=pid
     return render_to_response('teacherhome.html',args)

#######################################################Session starts here################################################################
def sessiondata(request):
    args = {}
    args.update(csrf(request))
    try:
       person=AuthUser.objects.get(email=request.session['email_id'])
    except Exception as e:
          args['error_message'] = "\n Error " + str(e.message) + str(type(e))
          return render(request,error_,args)
    

    args['firstname']=person.firstname
    args['lastname']=person.lastname
    args['email']=request.session['email_id']
    args['courseid']= request.session['courseid']
    
    args['edxcourseid']=request.session['edxcourseid']
    args['rooturl']=ROOT_URL
    return args            

##################################################Start ofparticipantdetails##############################################################
def studentdetails(request,courseid,pid):
      email=request.user
      args={}
      student_list=AuthUser.objects.raw('''select "1" id ,au.email email,au.username username from student_courseenrollment sce , auth_user au where sce.course_id=%s and sce.user_id not in (select user_id from student_courseaccessrole scr where course_id =%s) and sce.user_id =au.id''',[courseid,courseid])
      #student_list=mysql_csr.fetchall()
      student_detail=[]
      args['email']=request.user
      args['institutename']=request.session['org']
      course=edxcourses.objects.get(courseid=courseid).course
      args['course']=course
      args['courseid']=courseid
      for i in student_list:
           student_detail.append([i.email,i.username])
      args['studentdetail']=student_detail
      return render_to_response('participantdetails.html',args)


def downloadcsv(request,courseid):
    name=courseid+"participant_details"+'.csv'
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=" %s"'%(name)
    context=RequestContext(request)
    writer = csv.writer(response)   
    writer.writerow(["Email","UserName"]) 
    courselevelid= edxcourses.objects.get(courseid=courseid).course
    for clid in courselevelid:
         students = AuthUser.objects.raw('''select "1" id ,au.email email,au.username username from student_courseenrollment sce , auth_user au where sce.course_id=%s and sce.user_id not in (select user_id from student_courseaccessrole scr where course_id =%s) and sce.user_id =au.id''',[courseid,courseid])
    for student in students:  
        writer.writerow([student.email,student.username])
        print writer,"hji"
    return response

#############################################End of participantdetails#####################################################################
def coursedetails(request,courseid,pid):
    args={}
    args['email']=request.user
    args['institutename']=request.session['org']
    course=edxcourses.objects.get(courseid=courseid)
    args['coursenm']=course.coursename
    args['course']=course.course
    args['coursestart']=course.coursestart.date()
    args['courseend']=course.courseend.date()
    args['enrollstart']=course.enrollstart.date()
    args['enrollend']=course.enrollend.date()
    policy=[["Assignment","Total","Mandatory","Weight(%)","Comments"]]
    criteria=[["Grade","Min %","Max %"]]
    evaluate=[["Assignment","Assignment Type","Due Date"]]
    try:
        grpolicy=gradepolicy.objects.filter(courseid__courseid=courseid)
        for gp in grpolicy:
            if gp.drop_count==0:
                policy.append([gp.type+' ('+ gp.short_label +')',gp.min_count,gp.min_count-gp.drop_count,(gp.weight *100),""])
            else:
                 policy.append([gp.type+' ('+ gp.short_label +')',gp.min_count,gp.min_count-gp.drop_count,(gp.weight * 100),"Best of "+str((gp.min_count-gp.drop_count))])
        grcriteria=gradescriteria.objects.filter(courseid__courseid=courseid).values('cutoffs','grade').order_by('cutoffs').reverse().distinct()
        l=len(grcriteria)
        for  gc in range(0,len(grcriteria)):
             # print grcriteria,gc
              if gc==0:
                 criteria.append([grcriteria[gc]['grade'],grcriteria[gc]['cutoffs']*100,100])
              else:
                 criteria.append([grcriteria[gc]['grade'],grcriteria[gc]['cutoffs']*100,grcriteria[gc-1]['cutoffs']*100])
        evaluat=evaluations.objects.filter(course__courseid=courseid).values('sectionid','sec_name','type','due_date').order_by('sectionid').distinct()
        for eva in evaluat:
            evaluate.append([eva['sec_name'],eva['type'],eva['due_date']])

    except Exception as e:
           args['error_message'] ="my name is khan"
           return render(request,error_,args)

    args['evaluate']=evaluate
    args['criteria']=criteria
    args['policy']=policy
    args['id'] = pid
    return render_to_response('coursedetails.html',args)
#########################################################start of evaluationdetails##################################################
def evaluationdetails (request,courseid,pid):
    args={}
    args.update(csrf(request))
    args['email']=request.user
    args['institutename']=request.session['org']
    courseobj = edxcourses.objects.get(courseid = courseid)
    args['coursename']=courseobj.coursename
    args['course']=courseobj.course
    args['courseid']=courseid
    args['pid']=pid
    evaluation_obj=evaluations.objects.filter(course=courseobj,due_date__lte=current).values('sectionid','sec_name').distinct().order_by('due_date')
    args['evaluation']=evaluation_obj
    return render_to_response('evaluationdetails.html',args)

def quizdata(request,courseid,pid):
    args =sessiondata(request)
    email=request.user
    args={}
    args.update(csrf(request))
    header=[]
    try:
       secid=request.POST['quiz']

       evalu=evaluations.objects.filter(sectionid=secid).values('sec_name').distinct()
       args['secname']=evalu[0]['sec_name']
    except Exception as e:
           return evaluationdetails(request,courseid,pid)
    try:
       courseobj = edxcourses.objects.get(courseid = courseid)
       args['coursename']=courseobj.coursename
       args['course']=courseobj.course
       args['courseid']=courseid
       args['pid']=pid
    except Exception as e:
           args['error_message'] ="IITBombayX course is not present."
           return render(request,args)
    
    try:
      head_str=headings.objects.get(section=secid).heading
      heading=map(str,head_str.split(","))
      print heading,"before "

    except Exception as e:
      print str(e.message),str(type(e))
    
    ques_dict={}
    stud_rec=[]
    marks_obj=markstable.objects.filter(section=secid)
    students=AuthUser.objects.raw('''select "1" id ,au.email email,au.username username from student_courseenrollment sce , auth_user au where sce.course_id=%s and sce.user_id not in (select user_id from student_courseaccessrole scr where course_id =%s) and sce.user_id =au.id''',[courseid,courseid])

    for studvalue in marks_obj:
            marks=studvalue.eval.split(",")
            total=studvalue.total
    for student in students:
            stud_rec.append([student.email,student.username,total,marks])
    args['email']=request.user
    args['institutename']=request.session['org']        
    args['headings']=heading              
    request.session['stud_rec']= stud_rec          
    args['stud_rec']=stud_rec
    return render_to_response('quizdata.html',args)



def downloadquizcsv(request,courseid,pid):
    print courseid,"gdkjihwd"
    args={}
    args.update(csrf(request))
    currenttime = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
    courseobj = edxcourses.objects.get(courseid = courseid)
    args['coursename']=courseobj.coursename
    args['course']=courseobj.course 
    
    head_str=headings.objects.get(section=secid).heading
    heading=map(str,head_str.split(","))
      #print heading,"before "

   
    name=  "quizreport"+"_"+str(courseobj.id)+"_"+str(pid)+"_"+currenttime+'.csv'
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=" %s"'%(name)
    context=RequestContext(request)
    writer = csv.writer(response)
    
    #header_data=request.session['heading']
    header_data = [h.replace('<br>', '\n') for h in heading]
    if int(pid) == -1:
       count=0
       teacherheader=[]
       for i in header_data:
           count = count +1
           if count == 2:
              teacherheader.append("Teacher")
              teacherheader.append(i)
           else:
               teacherheader.append(i)
       writer.writerow(teacherheader)
       for data in result:
                count=0
                createrow=[]
                for row in data:
                    count=count+1
                    if count==6:
                       for r in row: 
                           
                           createrow.append(r)
                       
                    else:
                         createrow.append(row)
                writer.writerow(createrow)
    else:   
          writer.writerow(header_data)
          for data in result:
                count=0
                createrow=[]
                for row in data:
                    count=count+1
                    if count==5:
                       for r in row: 
                           
                           createrow.append(r)
                       
                    else:
                         createrow.append(row)
                writer.writerow(createrow)
    return response
######################################################end of evaluationdetails########################################################


#######################################################start of evaluationstatus######################################################
def evaluationstatus(request,courseid,pid,evalflag):
    args={}
    args.update(csrf(request))
    courseobj = edxcourses.objects.get(courseid = courseid)
    args['coursename']=courseobj.coursename
    args['course']=courseobj.course
    args['courseid']=courseid
    args['pid']=pid
    
    evaluation_obj=evaluations.objects.filter(course=courseobj,release_date__lte=current,due_date__lte=current).values('sectionid','sec_name').distinct().order_by('due_date')

    if evalflag==1:
        args['error_message'] = "Please select any quiz"
    
    args['evaluation']=evaluation_obj
    return render(request,'evaluationstatus.html',args)

def studentstatus(request,courseid,pid,report):
    
    args={}
    if request.POST:
           if request.POST['status']=="Select":
              return evaluationstatus(request,courseid,pid,1)
           request.session['secid']=request.POST['status']
    args.update(csrf(request))
    header=[]

    try:
        if int(pid)==-1:
            heading=['Student Email']
            args['teacher']="All Teachers"        
        else:
           heading=['Student Email']
    except Exception as e:
           args['error_message'] ="You are not valid Teacher for the course ",courseid
           args['error_message'] = "\n Error " + str(e.message) + str(type(e))
           return render(request,error_,args)
    try:
       secid=request.session['secid']
       marks_obj=[]
       marks=markstable.objects.filter(section=secid,edx_user=pid)
       for i in marks:             
           marks_obj.append(i)
    except Exception as e:
           return evaluationstatus(request,courseid,pid,1)
    
    courseobj = edxcourses.objects.get(courseid = courseid)
    args['coursename']=courseobj.coursename
    args['course']=courseobj.course
    args['courseid']=courseid
    args['pid']=pid
    #args['error_message'] ="IITBombayX course is not present."
       #args['error_message'] = "\n Error " + e.message + type(e)
    #return render(request,error_,args)    
    args['secname']=evaluations.objects.filter(sectionid=secid)[0].sec_name
    ques_dict={}
    NA_stud_rec=[]; PA_stud_rec=[]; AA_stud_rec=[]
    NA_count=0; PA_count=0; AA_count=0
    
    for studvalue in marks_obj:            
            if studvalue.total=="NA":               
               NA_count=NA_count+1
               if int(pid)==-1:
                   NA_stud_rec.append([str(studvalue.stud.roll_no),str(studvalue.stud.teacherid.personid.email),str(studvalue.stud.edxuserid.email)])
               else:
                   NA_stud_rec.append([str(studvalue)])
            elif  studvalue.total!="NA" and "NA" in studvalue.eval:                  
                  PA_count=PA_count+1
                  if int(pid)==-1:
                      PA_stud_rec.append([str(studvalue.stud.teacherid.personid.email),str(studvalue.stud.edxuserid.email)])
                  else:
                      PA_stud_rec.append([str(studvalue.stud.edxuserid.email)])

            else:                 
                 AA_count=AA_count+1
                 if int(pid)==-1:
                     AA_stud_rec.append([str(studvalue.stud.teacherid.personid.email),str(studvalue.stud.edxuserid.email)])
                 else:
                      AA_stud_rec.append([str(studvalue.stud.edxuserid.email)])
               
    
    args['NA_count']= NA_count; args['PA_count']= PA_count ; args['AA_count']= AA_count

    if int(report) == 0:
         return render(request,'studentstatus.html',args) 
    elif int(report) == 5:
          args['stud_rec']= NA_stud_rec          
    elif int(report) == 6:          
          args['stud_rec']= PA_stud_rec           
    else:
          args['stud_rec']= AA_stud_rec 

    args['filename']='tmp/'+downloadstatucsv(request,courseid,pid,report,args['stud_rec'])    
    full_path = os.path.abspath(os.path.join(os.path.dirname(__file__),os.pardir))
    request.session['stud_rec']=args['stud_rec']
    args['stud_rec']=stud_rec           
    args['heading']= heading
    command = "python "+full_path+"/manage.py collectstatic  --noinput"
    subprocess.call(command, shell=True)
    return render(request,'studentstatus.html',args)
  

'''def downloadstatucsv(request,courseid,pid,report,stud_rec):
    args={}
    currenttime = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
    try:
       courseobj = edxcourses.objects.get(courseid = courseid)
       args['coursename']=courseobj.coursename
       args['course']=courseobj.course
      
    except Exception as e:
           args['error_message'] = getErrorContent("no_IITBombayX_course")
           args['error_message'] = "\n Error " + e.message + type(e)
           return render(request,error_,args)
    full_path = os.path.abspath(os.path.join(os.path.dirname(__file__),os.pardir))
    
    if int(report) == 5:
       realname= "student_notattempted_"+str(courseobj.id)+"_"+str(pid)+'.csv'
       
    elif int(report) == 6:
       realname= "student_attemptsomeques_"+str(courseobj.id)+"_"+str(pid)+'.csv'
    else:
       realname= "student_attemptall_"+str(courseobj.id)+"_"+str(pid)+'.csv'
    name=os.path.join(full_path,'static/tmp/',realname)
    print name
    with open(name,"wb") as downloadfile:
         writer=csv.writer(downloadfile,delimiter=',')
         if int(pid)==-1:
            heading=['Teacher','Student Email']
         else:
              heading=['Student Email']
         writer.writerow(heading)
         for row in  stud_rec:
             writer.writerow(row)
    
    return realname'''
#############################################################end of evaluationstatus#####################################################


#########################################################Start of grade detail########################################################
def grades_report(request,courseid,pid):
    args={}
    args.update(csrf(request))
    course = edxcourses.objects.get(courseid = courseid).course
    header=[] 
    student_record=[]
    try: 
       header=headings.objects.get(section=course).heading
    except Exception as e:
       print "Header does not exists",str(e.message),str(type(e))
    header_data=map(str,header.split(","))
    gradestable_obj=gradestable.objects.filter(course=course)
    student_details=AuthUser.objects.raw('''select "1" id ,au.email email,au.username username from student_courseenrollment sce , auth_user au where sce.course_id=%s and sce.user_id not in (select user_id from student_courseaccessrole scr where course_id =%s) and sce.user_id =au.id''',[courseid,courseid])

    for grades in gradestable_obj:
           marks=grades.eval.split(",")
           grade=grades.grade
    for student_detail in student_details:     
           student_record.append([student_detail.email,student_detail.email,grade,marks]) 	 	
    args['headings']=header_data                
    args['student_record']=student_record
    args['course']=course
    args['pid']=pid
    args['email']=request.user
    args['institutename']=request.session['org']        

    
    return render(request,'course_grades.html',args) 

##################################################End of grade detail################################################################

################################################Start of course module structure#########################################################
def get_course(request):
    args={}
    courses=course_modlist.objects.filter(module_type="course")
    course_list=[]
    for course in courses:
        course_list.append([course.display_name,course.module_id])
    args['courses']=course_list
    return render(request,getcourse_,args)

def course_chapter(request):
        course_id=request.GET['id']
        course=course_modlist.objects.get(module_id=course_id)
        chapters_list=[]
        chapters=course_modlist.objects.filter(related_id=course.id)
        for chapter in chapters:
            chapters_list.append([chapter.display_name,chapter.id])
        return HttpResponse(json.dumps(chapters_list), content_type="application/json")

def chapter_sequential(request):
       chapter_id=request.GET['id']
       sequential_list=[]
       sequentials=course_modlist.objects.filter(related_id=chapter_id)
       for sequential in sequentials:
           sequential_list.append([sequential.display_name,sequential.id])
       return HttpResponse(json.dumps(sequential_list), content_type="application/json")


def sequential_vertical(request):
     sequential_id= request.GET['id']
     vertical_list=[]
     verticals=course_modlist.objects.filter(related_id=sequential_id)
     for vertical in verticals:
         vertical_list.append([vertical.display_name,vertical.id])
     return HttpResponse(json.dumps(vertical_list), content_type="application/json")

def vertical_module(request):
     vertical_id= request.GET['id']
     module_list=[]
     modules=course_modlist.objects.filter(related_id=vertical_id).values("module_type").distinct()
     for module in modules:
         moduletype=module['module_type']
         module_list.append([moduletype,vertical_id])
         print module_list
     return HttpResponse(json.dumps(module_list), content_type="application/json")
###############################################End of course module structure##############################################################


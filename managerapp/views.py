from django.shortcuts import render
from login.views import *
from iitbx.models import *
from login.models import *

def reporthome(request):
     args={}
     args['institutename']=request.session['org']
     args['email']=request.user
     return render_to_response('reporthome.html',args)

def usersummary(request):
    args={}
    email=request.user
    summary=AuthUser.objects.raw('''SELECT "1" id,count(*) Total,sum(Active) Active, sum(NotActive)  NotActive ,sum(Cur_Year) Cur_Year, sum(Cur_Month) Cur_Month ,sum(Cur_Quarter) Cur_Quarter ,sum(Cur_Week) Cur_Week ,sum(Yesterday) Yesterday,sum(Never) "NeverLogged", sum(LoggedIn) "LoggedIn",sum(ActiveCurYear) ActiveCurYear, sum(ActiveCurMonth) "ActiveCurMonth",sum(ActiveCurWeek) "ActiveCurWeek", sum(ActiveYesterday) "ActiveYesterday",sum(NoEnrollment) "NoEnrollment",
sum(SingleCourse) "SingleCourse",sum(MultipleCourse) "MultipleCourse"
FROM
(SELECT if(is_active=1,1,0) "Active"
      ,if(is_active=0,1,0) "NotActive"
      ,if(is_active=1 and YEAR(date_joined)=YEAR(current_date()),1,0) "Cur_Year"
      ,if(is_active=1 and YEAR(date_joined)=YEAR(current_date()) and QUARTER(date_joined)=QUARTER(current_date()),1,0) "Cur_Quarter"
      ,if(is_active=1 and YEAR(date_joined)=YEAR(current_date()) and MONTH(date_joined)=MONTH(current_date()),1,0) "Cur_Month"
,if(is_active=1 and YEAR(date_joined)=YEAR(current_date()) and WEEK(date_joined)=WEEK(current_date()),1,0) "Cur_Week"
,if(date_joined > current_date()-1,1,0) "Yesterday"
,if (is_active=1 and TotalEnrolled =0,1,0) "NoEnrollment"
,if (is_active=1 and TotalEnrolled =1,1,0) "SingleCourse"
,if (is_active=1 and TotalEnrolled >1,1,0) "MultipleCourse"
,if(last_login= date_joined and is_active=1,1,0) "Never",
if(last_login!= date_joined and is_active=1,1,0) "LoggedIn",
if(is_active=1 and last_login!= date_joined and year(last_login) = year( current_date()),1,0) "ActiveCurYear",
if(is_active=1 and last_login!= date_joined and date_format(last_login,'%%m-%%y') = date_format( current_date(),'%%m-%%y'),1,0) "ActiveCurMonth"
,if(is_active=1 and last_login!= date_joined and date_format(last_login,'%%m-%%y') = date_format( current_date(),'%%m-%%y') and week(last_login)=week(date_joined),1,0) "ActiveCurWeek" ,
 if(is_active=1 and last_login!= date_joined and date_format(last_login,'%%d-%%m-%%y') = date_format( current_date()-1,'%%d-%%m-%%y'),1,0) "ActiveYesterday"
FROM (SELECT username,email,date_joined,b.is_active,last_login,sum(if(course_id IS NULL,0,1)) "TotalEnrolled" FROM `student_courseenrollment` a RIGHT OUTER JOIN auth_user b ON b.id=a.user_id group by username,email,date_joined,b.is_active,last_login
) b)
a
''')
    for i in summary:
        args["total"]=i.Total
        args["active"]=i.Active
        args["notactive"]=i.NotActive
        args["Cur_Year"]=i.Cur_Year
        args["Cur_Month"]=i.Cur_Month
        args["Cur_Quarter"]=i.Cur_Quarter
        args["Cur_Week"]=i.Cur_Week
        args["Yesterday"]=i.Yesterday
        args["NeverLogged"]=i.NeverLogged
        args["LoggedIn"]=i.LoggedIn
        args["ActiveCurYear"]=i.ActiveCurYear
        args["ActiveCurMonth"]=i.ActiveCurMonth
        args["ActiveCurWeek"]=i.ActiveCurWeek
        args["SingleCourse"]=i.SingleCourse
        args["MultipleCourse"]=i.MultipleCourse
        args["ActiveYesterday"]=i.ActiveYesterday
        args["NoEnrollment"]=i.NoEnrollment
        args['email']=request.user
        args['institutename']=request.session['org']
    return render_to_response('home1.html',args)

def registrationsummary(request):
    args={}
    regdata=[]
    email=request.user
    datejoin=AuthUser.objects.raw('''SELECT  "1" id,DATE_FORMAT( date_joined, '%%m-%%Y' ) doj , count( * ) "Total"  FROM auth_user where is_active=1 GROUP BY DATE_FORMAT( date_joined, '%%m-%%Y' )''')
    for j in datejoin:
         regdata.append([j.doj ,j.Total])
    args["regdata"]=regdata
    return render_to_response('registrationsummery.html',args) 

def userjoinedsummary(request):
    args={}
    userdata=[]
    email=request.user
    userjoin=AuthUser.objects.raw('''SELECT "1" id, DATE_FORMAT(date_joined, '%%d-%%m-%%Y') doj ,count(*) "Total" from auth_user where date_joined < current_date() -7 and is_active=1 group by DATE_FORMAT(date_joined, '%%d-%%m-%%Y') ''')
    for j in userjoin:
         userdata.append([j.doj ,j.Total])
    args["userdata"]=userdata
    return render_to_response('userjoinedsummary.html',args) 

def coursesummary(request):
    args={}
    email=request.user
    coursedata=[]
    coursecount=AuthUser.objects.raw('''select "1" id,count(user_id) ui,course_id ci from student_courseenrollment group by course_id''')
    for k in coursecount:
        coursedata.append([k.ui,k.ci])
    args["coursedata"]=coursedata
    return render_to_response('coursesummary.html',args) 


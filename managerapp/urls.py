from django.conf.urls import url

from . import views

urlpatterns = [
     url(r'^manager/reporthome$', 'managerapp.views.reporthome',name='reporthome'),
     url(r'^manager/usersummary$', 'managerapp.views.usersummary',name='usersummary'),
     url(r'^manager/registrationsummary$', 'managerapp.views.registrationsummary',name='registrationsummary'),
     url(r'^manager/userjoinedsummary$', 'managerapp.views.userjoinedsummary',name='userjoinedsummary'),
     url(r'^manager/coursesummary$', 'managerapp.views.coursesummary',name='coursesummary'),
    
 ]
